package com.regeneration.academy;

import com.regeneration.academy.dao.BatchesExample;
import com.regeneration.academy.dao.CreateSchemaExample;
import com.regeneration.academy.dao.CreateTablesExample;
import com.regeneration.academy.dao.DeleteExample;
import com.regeneration.academy.dao.InsertExample;
import com.regeneration.academy.dao.ResultSetExample;
import com.regeneration.academy.dao.TransactionExample;
import com.regeneration.academy.dao.UpdateExample;

public class CreateReadUpdateDelete {

    public static void main(String[] args) throws Exception {
        CreateSchemaExample.main(args);
        CreateTablesExample.main(args);
        InsertExample.main(args);
        ResultSetExample.main(args);
        UpdateExample.main(args);
        DeleteExample.main(args);
        TransactionExample.main(args);
        BatchesExample.main(args);
    }
}
