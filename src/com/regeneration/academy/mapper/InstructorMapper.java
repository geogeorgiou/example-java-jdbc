package com.regeneration.academy.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.regeneration.academy.domain.Instructor;

public class InstructorMapper {

    public Instructor extractInstructor(ResultSet resultSet) throws SQLException {
        Instructor instructor = new Instructor();
        instructor.setId(resultSet.getInt("id"));
        instructor.setFirst(resultSet.getString("first"));
        instructor.setLast(resultSet.getString("last"));
        instructor.setAge(resultSet.getInt("age"));
        instructor.setCourse(resultSet.getString("course"));
        return instructor;
    }

}
