package com.regeneration.academy.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class CreateSchemaExample {

    // JDBC driver name and database URL
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_BASE_URL = "jdbc:mysql://localhost:3306/";
    private static final String DB_SCHEMA = "example_db";


    // Database credentials
    private static final String USER = "root";
    private static final String PASS = "root";

    public static void main(String[] args) throws Exception {
        Class.forName(JDBC_DRIVER);

        Connection conn = DriverManager.getConnection(DB_BASE_URL, USER, PASS);
        Statement stmt = conn.createStatement();

        String sql = "create schema if not exists " + DB_SCHEMA + ";";

        stmt.executeUpdate(sql);

        conn.close();
    }
}