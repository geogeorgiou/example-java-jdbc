package com.regeneration.academy.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class InsertExample {

    // JDBC driver name and database URL
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_BASE_URL = "jdbc:mysql://localhost:3306/%s";
    private static final String DB_SCHEMA = "example_db";
    private static final String DB_URL = String.format(DB_BASE_URL, DB_SCHEMA);

    // Database credentials
    private static final String USER = "root";
    private static final String PASS = "root";

    public static void main(String[] args) throws Exception {
        Class.forName(JDBC_DRIVER);

        Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
        Statement stmt = conn.createStatement();

        String query1 = "INSERT INTO Instructor (first, last, age, course) VALUES ('Chris', 'Gavanas', 26, 'Databases')";
        String query2 = "INSERT INTO Instructor (first, last, age, course) VALUES ('Christos', 'Peristeris', 35, 'I/O')";
        String query3 = "INSERT INTO Instructor (first, last, age, course) VALUES ('Spyros', 'Argyroiliopoulos', 35, 'Exception Handling')";
        String query4 = "INSERT INTO Instructor (first, last, age, course) VALUES ('Grigoris', 'Dimopoulos', 25, 'Threads')";
        String query5 = "INSERT INTO Instructor (first, last, age, course) VALUES ('Yiannis', 'Vlahopoulos', 28, 'Inheritance')";
        stmt.executeUpdate(query1);
        stmt.executeUpdate(query2);
        stmt.executeUpdate(query3);
        stmt.executeUpdate(query4);
        stmt.executeUpdate(query5);


        String queryFormat = "INSERT INTO Player (username, password) VALUES ('Spyros-%s', 'password-%s')";
        for (int i = 1; i <= 10; i++) {
            String query = String.format(queryFormat, i, i);
            stmt.executeUpdate(query);
        }


        conn.close();
    }
}