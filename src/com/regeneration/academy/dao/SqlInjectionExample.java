package com.regeneration.academy.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class SqlInjectionExample {

    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_BASE_URL = "jdbc:mysql://localhost:3306/%s";
    private static final String DB_SCHEMA = "example_db";
    private static final String DB_URL = String.format(DB_BASE_URL, DB_SCHEMA);

    //  Database credentials
    private static final String USER = "root";
    private static final String PASS = "root";

    public static void main(String[] args) throws Exception {
        Class.forName(JDBC_DRIVER);

        Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
        Statement stmt = conn.createStatement();

        String query = "SELECT first, last, age, course FROM Instructor where age > '%s'";

        Scanner sc = new Scanner(System.in);

        ResultSet rs = stmt.executeQuery(String.format(query, sc.nextLine()));
        while (rs.next()) {
            System.out.println(rs.getString("first") + " " + rs.getString("last") + " " + rs.getInt("age") + " " + rs.getString("course"));
        }

//        10000' UNION ALL SELECT username as first, password as last, id as age, password as course FROM Player --
//        PreparedStatement preparedStatement = conn.prepareStatement("SELECT first, last, age, course FROM Instructor where age > ?");
//        preparedStatement.setString(1, sc.nextLine());
//        ResultSet rs = preparedStatement.executeQuery();
//        while (rs.next()) {
//            System.out.println(rs.getString("first") + " " + rs.getString("last") + " " + rs.getInt("age") + " " + rs.getString("course"));
//        }

    }

}
