package com.regeneration.academy.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class UpdateExample {

    // JDBC driver name and database URL
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_BASE_URL = "jdbc:mysql://localhost:3306/%s";
    private static final String DB_SCHEMA = "example_db";
    private static final String DB_URL = String.format(DB_BASE_URL, DB_SCHEMA);

    // Database credentials
    private static final String USER = "root";
    private static final String PASS = "root";

    public static void main(String[] args) throws Exception {
        Class.forName(JDBC_DRIVER);

        Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
        Statement stmt = conn.createStatement();

        String updateQuery = "UPDATE Instructor SET age = 30 WHERE id in (1, 6, 25)";
        System.out.println("Updated " + stmt.executeUpdate(updateQuery) + " rows");

        conn.close();
    }
}