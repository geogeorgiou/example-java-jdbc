package com.regeneration.academy.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import com.regeneration.academy.domain.Instructor;


public class ResultSetExample {
    // JDBC driver name and database URL
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_BASE_URL = "jdbc:mysql://localhost:3306/%s";
    private static final String DB_SCHEMA = "example_db";
    private static final String DB_URL = String.format(DB_BASE_URL, DB_SCHEMA);

    // Database credentials
    private static final String USER = "root";
    private static final String PASS = "root";

    public static void main(String[] args) throws Exception {
        Class.forName(JDBC_DRIVER);

        Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
        Statement stmt = conn.createStatement();

        List<Instructor> instructors = new LinkedList<>();

        String query = "SELECT * FROM Instructor";
        ResultSet resultSet = stmt.executeQuery(query);
        while (resultSet.next()) {
            Instructor instructor = new Instructor();
            instructor.setId(resultSet.getInt("id"));
            instructor.setFirst(resultSet.getString("first"));
            instructor.setLast(resultSet.getString("last"));
            instructor.setAge(resultSet.getInt("age"));
            instructor.setCourse(resultSet.getString("course"));
            instructors.add(instructor);
        }

        conn.close();

        for (Instructor instructor : instructors) {
            System.out.println(instructor.toString());
        }
    }
}
