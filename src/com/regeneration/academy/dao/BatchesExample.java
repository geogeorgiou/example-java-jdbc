package com.regeneration.academy.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class BatchesExample {

    // JDBC driver name and database URL
    private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_BASE_URL = "jdbc:mysql://localhost:3306/%s";
    private static final String DB_SCHEMA = "example_db";
    private static final String DB_URL = String.format(DB_BASE_URL, DB_SCHEMA);

    // Database credentials
    private static final String USER = "root";
    private static final String PASS = "root";

    public static void main(String[] args) throws Exception {
        Class.forName(JDBC_DRIVER);

        Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
        conn.setAutoCommit(false);

        Statement stmt = conn.createStatement();

        String query1 = "UPDATE Instructor SET age = 30 WHERE id in (1, 6, 25)";
        String query2 = "UPDATE Player SET username = 'Hello' WHERE id = 1";
        String query3 = "DELETE From Instructor WHERE id < 10";

        stmt.addBatch(query1);
        stmt.addBatch(query2);
        stmt.addBatch(query3);
        int[] modifiedRows = stmt.executeBatch();
        System.out.println("Updated " + modifiedRows[0] + " rows, " +
                "Updated " + modifiedRows[1] + " rows, " +
                "Deleted " + modifiedRows[2] + " rows.");

        conn.commit();
        conn.close();
    }

}
