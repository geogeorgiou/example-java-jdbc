# In class example 4 - JDBC

Perform basic CRUD operations with Java and JDBC


You can download the `mysql-connector-java` .jar yourself directly from mysql site.
- https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-8.0.18.zip

Extract the zip content and copy the .jar under the `/libs` folder